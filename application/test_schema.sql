DROP TABLE kategoria;

CREATE TABLE kategoria(
  id_kategoria INTEGER AUTO_INCREMENT PRIMARY KEY,
  nazwa VARCHAR(50) NOT NULL,
  id_nadkategoria INTEGER references kategoria(id_kategoria)
);