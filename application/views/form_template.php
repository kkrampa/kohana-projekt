<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 11.05.14
 * Time: 11:10
 */
echo Form::open("", array("role" => "form"));
foreach($form_elements as $key => $element) {
    if(Arr::get($errors, $element['params']['name']) == NULL)
        echo '<div class="form-group">';
    else {
        echo '<div class="form-group has-error">';
        $error = Arr::get($errors, $element['params']['name']);
        echo '<span class="help-block">'.$error.'</span>';
    }
    if(!isset($element['label']))
        $label = strtoupper($key[0]).substr($key, 1);
    else
        $label = strtoupper($element['label'][0]).substr($element['label'], 1);
    echo Form::label($element['params']['name'], $label.": ", array("class" => 'control-label'));
    $element['params']['attributes'] = array("class" => "form-control");
    echo Form::control_form($element['type'], $element['params']);
    echo '</div>';
}
echo Form::submit("submit", "Wyślij",  array("class" => "btn btn-default"));
echo Form::close();
//var_dump($form_elements);