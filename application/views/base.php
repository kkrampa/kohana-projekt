<html>
	<head>
		<title><?php echo $title ?></title>
        <script src="//code.jquery.com/jquery-1.9.1.js"></script>
        <?php echo HTML::style('/media/css/bootstrap.min.css'); ?>
        <?php echo HTML::script('/media/js/bootstrap.min.js') ?>

        <style>
            body {
                width: 1024px;
                margin: 0 auto;
            }

            form {
                width: 500px;
            }

        </style>

    </head>
	<body>
		<?php echo $content ?>
	</body>
</html>