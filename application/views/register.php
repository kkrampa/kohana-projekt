<?php
echo Form::open("auth/register", array("role" => "form")) ?>
    <fieldset>
        <legend>Rejestracja</legend>
    <?php
    if(isset($errors)) {
        echo Arr::get($errors, 'nazwa');
        echo "<br />";
    }
    $error = Arr::get($errors, 'username');
    if($error == NULL)
        echo '<div class="form-group">';
    else {
        echo '<div class="form-group has-error">';

        echo '<span class="help-block">'.$error.'</span>';
    }

    echo Form::label('username', 'Login', array("class" => 'control-label'));
    echo Form::input('username', $user->username, array("class" => "form-control"));
    echo '</div>';


    $error = Arr::get($errors, 'email');
    if($error == NULL)
        echo '<div class="form-group">';
    else {
        echo '<div class="form-group has-error">';

        echo '<span class="help-block">'.$error.'</span>';
    }

    echo Form::label('email', 'Email', array("class" => 'control-label'));
    echo Form::input('email', $user->email, array("class" => "form-control"));
    echo '</div>';

    $error = Arr::path($errors, '_external.password');
    if($error == NULL)
        echo '<div class="form-group">';
    else {
        echo '<div class="form-group has-error">';

        echo '<span class="help-block">'.$error.'</span>';
    }
    echo Form::label('password', 'HasĹo', array("class" => 'control-label'));
    echo Form::password('password', NULL, array("class" => "form-control"));
    echo '</div>';

    $error = Arr::path($errors, '_external.password_confirm');
    if($error == NULL)
        echo '<div class="form-group">';
    else {
        echo '<div class="form-group has-error">';

        echo '<span class="help-block">'.$error.'</span>';
    }

    echo Form::label('password_confirm', 'PowtĂłrz hasĹo', array("class" => 'control-label'));
    echo Form::password('password_confirm', NULL, array("class" => "form-control"));
    echo '</div>';


    echo Form::submit("submit", "Submit",  array("class" => "btn btn-default"));
    echo Form::close();
    ?>