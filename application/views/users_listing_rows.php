
<thead>
<tr>
    <?php
        foreach($columns as $column) {
            echo "<th>$column</th>";
        }
    ?>
</thead>

<?php
foreach($object_list as $key => $value) {
    echo "<tr>";
    foreach ($columns as $column) {
        if($column == 'roles') {
            $roles = $value->roles->find_all();
            echo "<td>";
            foreach($roles as $role) {
                echo $role->name;
            }
            echo "</td>";
        } else {
            echo "<td>".$value->get("$column")."</td>";
        }
    }
    echo "</tr>";
}