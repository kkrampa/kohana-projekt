<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Kategoria extends ORM {
	protected $_table_name = "kategoria";
    protected $_primary_key = "id_kategoria";
    protected $_table_columns = array("id_kategoria" => NULL,
        "nazwa" => NULL, "id_nadkategoria" => NULL);
	protected $_belongs_to = array('nadkategoria' => array('model' => 'kategoria', 'foreign_key' => 'id_nadkategoria'));

	public function __toString()
	{
    	return (string) $this->nazwa;
	}

    public function rules()
    {
        return array(
            'nazwa' => array(
                array('not_empty')

            )

        );
    }
}
