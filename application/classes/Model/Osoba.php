<?php


class Model_Osoba extends ORM {
	protected $_table_name = 'osoba';
	protected $_primary_key = 'id_osoba';
    protected $_table_columns = array("imie" => NULL, "nazwisko" => NULL);

    public function rules()
    {
        return array(
            'imie' => array(
                array('not_empty')

            ), 'nazwisko' => array(
                array('not_empty')
            )

        );
    }
}
