<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 10.05.14
 * Time: 21:47
 */

class Model_User extends Model_Auth_User
{
    protected $_table_columns =  array(
        'id' => NULL,
        'email' => NULL,
        'username' => NULL,
        'password' => NULL,
        'logins' => NULL,
        'last_login' => NULL,
    );

}