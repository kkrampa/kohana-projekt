<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 10.05.14
 * Time: 22:12
 */
    class Form extends Kohana_Form {

        private static function get_element_or_null(array $array, $key) {
            return (isset($array[$key])) ? $array[$key] : NULL;
        }


        public static function control_form($name, array $params) {
            switch($name) {
                case "input":
                    return Form::input(Form::get_element_or_null($params, 'name'),
                        Form::get_element_or_null($params, 'value'),
                        Form::get_element_or_null($params, 'attributes'));
                case "password":
                    return Form::password(Form::get_element_or_null($params, 'name'),
                        Form::get_element_or_null($params, 'value'),
                        Form::get_element_or_null($params, 'attributes'));
                case "select":
                    return Form::select(Form::get_element_or_null($params, 'name'),
                        Form::get_element_or_null($params, 'options'),
                        Form::get_element_or_null($params, 'selected'),
                        Form::get_element_or_null($params, 'attributes')
                    );
            }
            return "test2";
        }



    }
