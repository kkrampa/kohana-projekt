<?php defined('SYSPATH') or die('No direct script access.');

class Controller_User_Kategorie extends Controller_Generic_Listing {

	public $model = "Kategoria";
	public $html_columns = array("id_kategoria", "nazwa", "nadkategoria");
    public $actions = array("add");


    public $form_elements = array(
        "nazwa" => array("type" => "input", "params" => array("name" => "nazwa")),
        "id_nadkategoria" => array("type" => "select", "label" => "nadkategoria", "params" => array("name" => "id_nadkategoria"))
    );

    public function __construct(Kohana_Request $request, Kohana_Response $response) {
        parent::__construct($request, $response);
        $array = ORM::factory($this->model)->find_all()->as_array("id_kategoria", "nazwa");
        $array = array("NULL" => "Brak") + $array;
        $this->form_elements["id_nadkategoria"]["params"]["options"] =
            $array;
    }
}