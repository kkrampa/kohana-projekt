<?php defined('SYSPATH') or die('No direct script access.');

class Controller_User_Osoby extends Controller_Generic_Listing {

	public $model = "Osoba";
	public $html_columns = array("imie", "nazwisko");
    public $actions = array("add");

    public $form_elements = array(
        "imie" => array("type" => "input", "params" => array("name" => "imie")),
        "nazwisko" => array("type" => "input", "params" => array("name" => "nazwisko"))
    );


} 
