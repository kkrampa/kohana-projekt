<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Users extends Controller_Generic_Listing {

    public $model = "User";
    public $html_columns = array("id", "username", "email", "roles");
    public $rows_view = "users_listing_rows";
    public $title = "Lista użytkowników";
}