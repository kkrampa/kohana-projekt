<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Generic_Listing extends Controller_Generic_Main {

	public $view = 'listing';
	public $object_name = "object_list";
	public $model = null;
	public $html_columns;
    public $form_elements;
    public $form_template = "form_template";
    public $rows_view = "listing_rows";
    public $actions = array();
    public $title = "Listing";

    public function  before() {
        parent::before();
        $user = Auth::instance()->get_user();
        if (!$user)
        {
            $session = Session::instance();
            $session->set('messages', array(array("message" => "Dostęp do tej strony wymaga zalogowania!", "style"=> "alert-danger")));
            $this->redirect('auth/login');
        }
    }

	public function action_index()
	{
		$object = ORM::factory($this->model);
		$this->template->content = View::factory($this->view, array(
			'rows' => View::factory($this->rows_view, array("$this->object_name" => $object->find_all(),
                    "columns" => $this->html_columns
                )),
            'controller' => $this->request->controller(),
            'actions' => $this->actions,
            'route' => Route::name($this->request->route()))

        );
    }

    public function action_add()
    {
        if(!in_array("add", $this->actions))
            throw new Kohana_HTTP_Exception_404("Akcja niezdefiniowana");
        $this->template->content = View::factory($this->form_template)->bind('form_elements',
            $this->form_elements);
        $this->template->content->bind('errors', $errors);
        if(HTTP_Request::POST == $this->request->method()) {
            $model_instance = ORM::factory($this->model);
            $model_instance->values($_POST);
            try {
                $model_instance->save();
                $this->redirect($this->request->controller());
            } catch(ORM_Validation_Exception $e) {
                $errors = $e->errors('models');
                foreach($this->form_elements as $key => $value) {
                    $type = $this->form_elements[$key]['type'];
                    if($type == 'input')
                        $this->form_elements[$key]['params']['value'] = $_POST[$key];
                    else if($type == 'select')
                        $this->form_elements[$key]['params']['selected'] = $_POST[$key];
                }
            }
        }

    }
} 