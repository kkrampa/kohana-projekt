<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_Generic_Main extends Controller_Template {
	
	public $template = 'base';
    public $title = 'Strona główna';
    public function  before() {
        parent::before();
        $this->template->bind("title", $this->title);
    }

} 