<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_Welcome extends Controller_Generic_Main {

	public function action_index()
	{
		$this->template->content = View::factory('main');
        $this->template->title = "Strona główna";
	}

}