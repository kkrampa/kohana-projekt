<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_Auth extends Controller_Generic_Main {


    public $title;


    public function action_login() {
        $this->title = 'Logowanie';

        if (Auth::instance()->logged_in()) {
            $session = Session::instance();
            $session->set('messages', array(array("message" => "JesteĹ juĹź zalogowany.", "style"=> "alert-info")));
            $this->redirect("user/");
        }
        if(isset($_POST['submit'])){
            $status = Auth::instance()->login($_POST['username'], $_POST['password']);
            if(!$status) {
                $session = Session::instance();
                $session->set('messages', array(array("message" => "PodaĹeĹ niepoprawne dane!", "style"=> "alert-danger")));
                $this->template->content = View::factory('main');

            } else {
                $session = Session::instance();
                $session->set('messages', array(array("message" => "Logowanie udane.", "style"=> "alert-success")));
                $this->redirect("user/");
            }
        } else {
            $this->template->content = View::factory('main');
        }

        //if (Auth::instance()->logged_in()) {
        //    echo 'zalogowany';
        //    echo Auth::instance()->get_user();
        //    if (Auth::instance()->logged_in('admin')) {
        //    echo 'to jest admin';
        //   }
        //}
        // else {
        //    echo "User is not logged in";
        //}
    }
    public function action_logout() {
        Auth::instance()->logout();
        $session = Session::instance();
        $session->set('messages', array(array("message" => "ZostaĹeĹ wylogowany.", "style"=> "alert-success")));
        $this->redirect('/');
    }
    public function action_register() {
        $this->title = 'Rejestracja';

        if (Auth::instance()->logged_in()) {
            $session = Session::instance();
            $session->set('messages', array(array("message" => "JesteĹ juĹź zalogowany.", "style"=> "alert-info")));
            $this->redirect("/");
        }
        $this->template->content = View::factory('register')->bind('errors', $errors)
            ->bind('user', $user_save);
        $this->template->title = "Rejestracja";
        if (HTTP_Request::POST == $this->request->method()) {
            try {
                $user = ORM::factory('User')->create_user($this->request->post(), array(
                    'username',
                    'password',
                    'email'
                ));

                $user->add('roles', ORM::factory('Role', array('name' => 'login')));
                //$_POST = array();

                $session = Session::instance();
                $session->set('messages', array(array("message" => "Twoje konto zostaĹo utworzone.
                MoĹźesz teraz siÄ zalogowaÄ.", "style"=> "alert-success")));
                $this->redirect("/auth/login");
            } catch(ORM_Validation_Exception $e) {
                $errors = $e->errors('models');
                $user_save = ORM::factory("User");
                $user_save->values($_POST);
            }
        } else {
            $errors = array();
            $user_save = ORM::factory('User');
        }
        /*
        $client = ORM::factory('user');
        $client->email = "admin@email.com";
        $client->username = "admin";
        $client->password = "admin";
        $client->save();
        */
        //$role = ORM::factory('role','1');
        //$client->add('roles',$role);
        //$client->save();
    }
}
?>